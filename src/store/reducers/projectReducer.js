const initState = {
    projects: [
        {id: '1', title: 'Backo je car', content: 'blah blah blah'},
        {id: '2', title: 'Backo je zmaj', content: 'blah blah blah'},
        {id: '3', title: 'Backo je kralj', content: 'blah blah blah'}
    ]

}

const projectReducer = (state = initState, action) => {
    switch(action.type){
        case 'CREATE_PROJECT':
            return state;
        case 'CREATE_PROJECT_ERROR':
        console.log('Create project error', action.err);
            return state;
        default:
            return state;
    }
    
}

export default projectReducer;