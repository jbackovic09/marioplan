import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// Initialize Firebase
var config = {
    apiKey: "AIzaSyDHM1urrMW5USb8s0PQHiu-KJ9cT91DqfU",
    authDomain: "net-ninja-marioplan-55e66.firebaseapp.com",
    databaseURL: "https://net-ninja-marioplan-55e66.firebaseio.com",
    projectId: "net-ninja-marioplan-55e66",
    storageBucket: "net-ninja-marioplan-55e66.appspot.com",
    messagingSenderId: "976388240727"
};


firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;